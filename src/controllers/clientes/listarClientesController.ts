import { prismaClient } from "../../database/prismaClient";
import { Request, Response } from "express";
import useTokenDecoded from "../../utils/useTokenDecoded";

export class ListarClientesController {
  async handle(request: Request, response: Response) {
    const { id } = useTokenDecoded(request);
    const { query } = request;

    let params = {};
    // Parâmetros de search da query
    if (query?.search) {
      params = {
        nome: {
          contains: String(query?.search),
          mode: "insensitive",
        },
      };
    }

    // Endpoint para listar todos os clientes
    const clientes = await prismaClient.cliente
      .findMany({
        where: {
          usuarioId: id,
          ...params,
        },
        select: {
          id: true,
          nome: true,
          cpfCnpj: true,
          email: true,
          telefone: true,
          dataNasc: true,
        },
        orderBy: {
          nome: "asc",
        },
      })
      .catch(() => {
        //Retornar erro caso os clientes não sejam listados
        throw new Error("Ocorreu um erro ao listar os clientes");
      });

    return response.json(clientes);
  }
}
